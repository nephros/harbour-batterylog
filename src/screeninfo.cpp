#include "screeninfo.h"

// -----------------------------------------------------------------------

//const QString ScreenInfo::BASE_PATH_SYSFS           = "/sys/class/leds/lcd-backlight/";
const QString ScreenInfo::BASE_PATH_SYSFS           = "/sys/class/backlight/panel0-backlight/";
const QString ScreenInfo::FILENAME_SYSFS_BRIGHTNESS = "brightness";

// -----------------------------------------------------------------------

ScreenInfo::ScreenInfo(QObject *parent) :
    QObject(parent), m_on(false)
{
}

// -----------------------------------------------------------------------

ScreenInfo::~ScreenInfo()
{
}

// -----------------------------------------------------------------------

void ScreenInfo::update()
{
    updateOn();
}

// -----------------------------------------------------------------------

void ScreenInfo::updateOn()
{
    // try to read sysfs brightness ...
    bool new_on = false;
    int brightness = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BRIGHTNESS, brightness))
        new_on = brightness > 0;

    if (new_on != m_on)
    {
        m_on = new_on;
        emit signalOnChanged();
    }
}
