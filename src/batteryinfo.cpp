#include "batteryinfo.h"

// -----------------------------------------------------------------------

const QString BatteryInfo::BASE_PATH_SYSFS                       = QStringLiteral("/sys/class/power_supply/");
const QString BatteryInfo::FILENAME_SYSFS_CAPACITY               = QStringLiteral("battery/capacity");
const QString BatteryInfo::FILENAME_SYSFS_CURRENT                = QStringLiteral("battery/current_now");
const QString BatteryInfo::FILENAME_SYSFS_VOLTAGE                = QStringLiteral("battery/voltage_now");

const QString BatteryInfo::FILENAME_SYSFS_BMS_CHARGE             = QStringLiteral("bms/charge_now_raw");
const QString BatteryInfo::FILENAME_SYSFS_BMS_CHARGE_FULL        = QStringLiteral("bms/charge_full");
const QString BatteryInfo::FILENAME_SYSFS_BMS_CHARGE_FULL_DESIGN = QStringLiteral("bms/charge_full_design");
const QString BatteryInfo::FILENAME_SYSFS_BMS_TTE                = QStringLiteral("bms/time_to_empty_avg");
const QString BatteryInfo::FILENAME_SYSFS_BMS_TTF                = QStringLiteral("bms/time_to_full_avg");
const QString BatteryInfo::FILENAME_SYSFS_BMS_VOLTAGE_OCV        = QStringLiteral("bms/voltage_ocv");

const QString BatteryInfo::FILENAME_SYSFS_STATUS                 = QStringLiteral("battery/status");
const QString BatteryInfo::FILENAME_SYSFS_HEALTH                 = QStringLiteral("battery/health");

const QString BatteryInfo::FILENAME_SYSFS_TEMP                   = QStringLiteral("battery/temp");

const QString BatteryInfo::UNKNOWN_STATUS                        = QStringLiteral("Unknown");
const QString BatteryInfo::UNKNOWN_HEALTH                        = QStringLiteral("Unknown");

// -----------------------------------------------------------------------

/*
 * from: https://github.com/sailfishos/statefs-providers/blob/master/src/power_udev/provider_power_udev.cpp#L320
 */

const long   BatteryInfo::reasonable_battery_voltage_min   = 1200000;  // NiCd/NiMH
const long   BatteryInfo::reasonable_battery_voltage_max   = 48000000; // 48V
const double BatteryInfo::battery_charging_voltage_default = 4200000;  // std Li-ion
// based on marketing Vnominal=3.7V (instead of real 3.6V) for
// batteries with Vcharging=4.2V
const double BatteryInfo::v_charging_to_nominal = 4200000 / (3700000 / 1000);

// -----------------------------------------------------------------------

BatteryInfo::BatteryInfo(QObject *parent) :
    QObject(parent), m_valid(false),
        m_capacity(0), m_current(0), m_voltage(0),
        m_energy(0), m_energy_full(0), m_energy_full_design(0),
        m_charge(0), m_charge_full(0), m_charge_full_design(0),
        m_status(UNKNOWN_STATUS), m_health(UNKNOWN_HEALTH)
{
}

// -----------------------------------------------------------------------

BatteryInfo::~BatteryInfo()
{
}

// -----------------------------------------------------------------------

void BatteryInfo::update()
{
    updateCapacity();
    updateCurrent();
    updateVoltage();
    // done from updateEnergy
    //updateVoltageOcv();
    //updateCharge();
    updateEnergy();
    updateStatus();
    updateTimes();
    updateTemp();
    updateHealth();
    updateValidity();
}

// -----------------------------------------------------------------------

void BatteryInfo::updateCapacity()
{
    int new_capacity = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_CAPACITY, new_capacity))
    {
        if (new_capacity != m_capacity)
        {
            m_capacity = new_capacity;
            emit signalCapacityChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateCurrent()
{
    int new_current = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_CURRENT, new_current))
    {
        new_current = qRound(new_current / 1000.0f);
        if (new_current != m_current)
        {
            m_current = new_current;
            emit signalCurrentChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateVoltage()
{
    int new_voltage = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_VOLTAGE, new_voltage))
    {
        new_voltage = qRound(new_voltage / 1000.0f);
        if (new_voltage != m_voltage)
        {
            m_voltage = new_voltage;
            emit signalVoltageChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateVoltageOcv()
{
    int new_voltage = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BMS_VOLTAGE_OCV, new_voltage))
    {
        new_voltage = qRound(new_voltage / 1000.0f);
        if (new_voltage != m_voltage_ocv)
        {
            m_voltage_ocv = new_voltage;
            emit signalVoltageOcvChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateEnergy()
{
    // make sure we are up to date
    updateVoltageOcv();
    updateCharge();

    /*
     * calculate Energy from charge and voltage, like StateFS
     *
     * from: https://github.com/sailfishos/statefs-providers/blob/master/src/power_udev/provider_power_udev.cpp#L914
     */

    // guard against bogus values
    if (m_voltage_ocv >= reasonable_battery_voltage_max || m_voltage_ocv <= reasonable_battery_voltage_min)
            m_voltage_ocv = battery_charging_voltage_default;

    // nominal voltage is calculated based on empiric constant. In
    // real life it should be taken from the battery spec
    //long nominal_voltage = charging_voltage * 1000 / v_charging_to_nominal;
    long nominal_voltage = m_voltage_ocv / v_charging_to_nominal;

    // full energy level
    int new_energy_full = 0;
    new_energy_full = qRound((m_charge_full * nominal_voltage) / 1000.0f );

    if (new_energy_full != m_energy_full)
    {
        m_energy_full = new_energy_full;
        emit signalEnergyFullChanged();
    }

    // design energy level (value for a brand new battery)
    int new_energy_full_design = 0;
    new_energy_full_design = qRound((m_charge_full_design * nominal_voltage) / 1000.0f );

    if (new_energy_full_design != m_energy_full_design)
    {
        m_energy_full_design = new_energy_full_design;
        emit signalEnergyFullDesignChanged();
    }

    // current energy level
    int new_energy = 0;
    new_energy = qRound((m_charge * nominal_voltage) / 1000.0f );

//    if (new_energy > new_energy_full)
//        new_energy = m_energy_full * m_capacity / 100.0f;

    if (new_energy != m_energy)
    {
        m_energy = new_energy;
        emit signalEnergyChanged();
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateCharge()
{
    // design charge level
    if (m_charge_full_design == 0) { // read only once, should be initialized with 0
        int new_charge_full_design = 0;
        if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BMS_CHARGE_FULL_DESIGN, new_charge_full_design))
        {
            new_charge_full_design = qRound(new_charge_full_design / 1000.0f);

            if (new_charge_full_design != m_charge_full_design)
            {
                m_charge_full_design = new_charge_full_design;
                emit signalChargeFullDesignChanged();
            }
        }
    }

    // full charge level
    int new_charge_full = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BMS_CHARGE_FULL, new_charge_full))
    {
        new_charge_full = qRound(new_charge_full / 1000.0f);

        if (new_charge_full != m_charge_full)
        {
            m_charge_full = new_charge_full;
            emit signalChargeFullChanged();
        }
    }

    // current charge level
    int new_charge = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BMS_CHARGE, new_charge))
    {
        new_charge = qRound(new_charge / 1000.0f);

        if (new_charge != m_charge)
        {
            m_charge = new_charge;
            emit signalChargeChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateTimes()
{
    int new_time = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BMS_TTE, new_time))
    {
        if (new_time != m_bms_tte)
        {
            m_bms_tte = new_time;
            emit signalTimeToEmptyChanged();
        }
    }

    new_time = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_BMS_TTF, new_time))
    {
        if (new_time != m_bms_ttf)
        {
            m_bms_ttf = new_time;
            emit signalTimeToFullChanged();
        }
    }
}


// -----------------------------------------------------------------------

void BatteryInfo::updateTemp()
{
    int new_temp = 0;
    if (readFileAsInteger(BASE_PATH_SYSFS + FILENAME_SYSFS_TEMP, new_temp))
    {
        if (new_temp != m_temp)
        {
            m_temp = new_temp;
            emit signalTempChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateStatus()
{
    QString new_status;
    if (readFileAsString(BASE_PATH_SYSFS + FILENAME_SYSFS_STATUS, new_status))
    {
        if (new_status.isEmpty())
            new_status = UNKNOWN_STATUS;

        QChar first_letter = new_status[0];
        new_status.remove(0, 1);
        new_status.prepend(first_letter.toUpper());

        if (new_status != m_status)
        {
            m_status = new_status;
            emit signalStatusChanged();
        }
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateHealth()
{
    QString new_health;
    readFileAsString(BASE_PATH_SYSFS + FILENAME_SYSFS_HEALTH, new_health);
    if (new_health.isEmpty())
        new_health = UNKNOWN_HEALTH;

    if (new_health != m_health)
    {
        m_health = new_health;
        emit signalHealthChanged();
    }
}

// -----------------------------------------------------------------------

void BatteryInfo::updateValidity()
{
    bool new_valid =
            m_status != UNKNOWN_STATUS &&
            //m_health != UNKNOWN_HEALTH &&
            m_capacity > 0 &&
            m_energy > 0 &&
            m_energy_full > 0 &&
            m_energy_full_design > 0 &&
            m_charge > 0;

    if (new_valid != m_valid)
    {
        m_valid = new_valid;
        emit signalValidChanged();
    }
}

