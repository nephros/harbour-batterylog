#ifndef BATTERYINFO_H
#define BATTERYINFO_H

#include <QObject>
#include <QString>

#include "infobase.h"

// -----------------------------------------------------------------------

class BatteryInfo : public QObject, public InfoBase
{
    Q_OBJECT

    Q_PROPERTY(bool valid READ isValid NOTIFY signalValidChanged)
    Q_PROPERTY(int capacity READ getCapacity NOTIFY signalCapacityChanged)
    Q_PROPERTY(int current READ getCurrent NOTIFY signalCurrentChanged)
    Q_PROPERTY(int voltage READ getVoltage NOTIFY signalVoltageChanged)
    Q_PROPERTY(int voltage_ocv READ getVoltageOcv NOTIFY signalVoltageOcvChanged)
    Q_PROPERTY(int energy READ getEnergy NOTIFY signalEnergyChanged)
    Q_PROPERTY(int energyFull READ getEnergyFull NOTIFY signalEnergyFullChanged)
    Q_PROPERTY(int energyFullDesign READ getEnergyFullDesign NOTIFY signalEnergyFullDesignChanged)
    Q_PROPERTY(int charge READ getCharge NOTIFY signalChargeChanged)
    Q_PROPERTY(int chargeFull READ getChargeFull NOTIFY signalChargeFullChanged)
    Q_PROPERTY(int chargeFullDesign READ getChargeFullDesign NOTIFY signalChargeFullDesignChanged)
    Q_PROPERTY(int timeToFull READ getTimeToFull NOTIFY signalTimeToFullChanged)
    Q_PROPERTY(int timeToEmpty READ getTimeToEmpty NOTIFY signalTimeToEmptyChanged)
    Q_PROPERTY(int temp READ getTemp NOTIFY signalTempChanged)
    Q_PROPERTY(QString status READ getStatus NOTIFY signalStatusChanged)
    Q_PROPERTY(QString health READ getHealth NOTIFY signalHealthChanged)

public:
    explicit BatteryInfo(QObject *parent = 0);
    virtual ~BatteryInfo();

    Q_INVOKABLE void update();

    bool isValid() const { return m_valid; }
    int getCapacity() const { return m_capacity; }
    int getCurrent() const { return m_current; }
    int getVoltage() const { return m_voltage; }
    int getVoltageOcv() const { return m_voltage_ocv; }
    int getEnergy() const { return m_energy; }
    int getEnergyFull() const { return m_energy_full; }
    int getEnergyFullDesign() const { return m_energy_full_design; }
    int getCharge() const { return m_charge; }
    int getChargeFull() const { return m_charge_full; }
    int getChargeFullDesign() const { return m_charge_full_design; }
    int getTimeToEmpty() const { return m_bms_tte; }
    int getTimeToFull() const { return m_bms_ttf; }
    int getTemp() const { return m_temp; }
    const QString &getStatus() const { return m_status; }
    const QString &getHealth() const { return m_health; }

signals:
    void signalValidChanged();
    void signalCapacityChanged();
    void signalCurrentChanged();
    void signalVoltageChanged();
    void signalVoltageOcvChanged();
    void signalEnergyChanged();
    void signalEnergyFullChanged();
    void signalEnergyFullDesignChanged();
    void signalChargeChanged();
    void signalChargeFullChanged();
    void signalChargeFullDesignChanged();
    void signalTimeToEmptyChanged();
    void signalTimeToFullChanged();
    void signalTempChanged();
    void signalStatusChanged();
    void signalHealthChanged();

private:
    static const QString BASE_PATH_SYSFS;
    static const QString FILENAME_SYSFS_CAPACITY;
    static const QString FILENAME_SYSFS_CURRENT;
    static const QString FILENAME_SYSFS_VOLTAGE;
    static const QString FILENAME_SYSFS_BMS_CHARGE;
    static const QString FILENAME_SYSFS_BMS_CHARGE_FULL;
    static const QString FILENAME_SYSFS_BMS_CHARGE_FULL_DESIGN;
    static const QString FILENAME_SYSFS_BMS_TTE;
    static const QString FILENAME_SYSFS_BMS_TTF;
    static const QString FILENAME_SYSFS_BMS_VOLTAGE_OCV;
    static const QString FILENAME_SYSFS_STATUS;
    static const QString FILENAME_SYSFS_HEALTH;

    static const QString FILENAME_SYSFS_TEMP;

    static const QString UNKNOWN_STATUS;
    static const QString UNKNOWN_HEALTH;

    void updateCapacity();
    void updateCurrent();
    void updateVoltage();
    void updateVoltageOcv();
    void updateCharge();
    void updateEnergy();
    void updateStatus();
    void updateTimes();
    void updateHealth();
    void updateTemp();
    void updateValidity();

    /*
     * from: https://github.com/sailfishos/statefs-providers/blob/master/src/power_udev/provider_power_udev.cpp#L320
     */

    static const long reasonable_battery_voltage_min;
    static const long reasonable_battery_voltage_max;
    static const double battery_charging_voltage_default;
    static const double v_charging_to_nominal;

    bool m_valid;
    int m_capacity;           // 0 - 100 %
    int m_current;            // [mA]
    int m_voltage;            // [mV]
    int m_voltage_ocv;        // [mV]
    int m_energy;             // [mWh]
    int m_energy_full;        // [mWh]
    int m_energy_full_design; // [mWh]
    int m_charge;             // [mAh]
    int m_charge_full;        // [mAh]
    int m_charge_full_design; // [mAh]
    int m_bms_tte;            // [seconds]
    int m_bms_ttf;            // [seconds]
    int m_temp;               // [deg C]
    QString m_status;
    QString m_health;

};

// -----------------------------------------------------------------------

#endif // BATTERYINFO_H
