import QtQuick 2.0
import Sailfish.Silica 1.0

Column {
    spacing: 0
    property alias title: titleLabel.text
    property alias value: valueLabel.text
    property alias note: noteLabel.text
    property int fontsize
    Label
    {
        id: titleLabel
        anchors.horizontalCenter: parent.horizontalCenter
        visible: text.length > 0
        //width: parent.width
        color: Theme.primaryColor
        horizontalAlignment: Text.AlignHCenter
        font { family: Theme.fontFamily; pixelSize: parent.fontsize ? parent.fontsize : Theme.fontSizeMedium }
    }
    Label
    {
        id: valueLabel
        anchors.horizontalCenter: parent.horizontalCenter
        visible: text.length > 0
        //width: parent.width
        color: Theme.secondaryColor
        horizontalAlignment: Text.AlignHCenter
        font { family: Theme.fontFamily; pixelSize: parent.fontsize ? parent.fontsize : Theme.fontSizeMedium }
    }
    Label
    {
        id: noteLabel
        anchors.horizontalCenter: parent.horizontalCenter
        visible: text.length > 0
        //width: parent.width
        color: Theme.secondaryHighlightColor
        horizontalAlignment: Text.AlignHCenter
        font { family: Theme.fontFamily; pixelSize: Theme.fontSizeTiny }
    }
}
